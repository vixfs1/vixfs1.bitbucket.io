function chooseLevelScreen() {
	const NewMenu = `
  	<div class="container start">
	  	<div class="d-flex flex-column bd-highlight mb-3 justify-content-between">
				<div class="p-2 bd-highlight flex-fill backlineimg">
				  	<div class="d-flex flex-column bd-highlight mb-3 gameinfo text-center justify-content-around">
				  		<div class="p-2 bd-highlight">
						  	<div class="d-flex flex-column bd-highlight mb-3">	
						  		<div class="p-2 bd-highlight">
						  			<p class="gamenumber2 ">Игра №2</p>
						  		</div>
						  		<div class="p-2 bd-highlight">
						  			<button class="secretsofgalaxy">
						  				Секреты <br>галактики
						  			</button>
						 		</div>
						 		<div class="p-2 bd-highlight">
						  			<p class="helponstart">Найди и открой все парные карточки,<br>чтобы узнать как выглядят наши<br> братья с других планет</p>
						 		</div>
						 	</div>
						</div>
				 		<div class="p-2 bd-highlight startcardimgs">
				  			<img src="img/startcard1.svg">
				  			<img src="img/startcard2.svg">
				  			<img src="img/startcard3.svg">
				 		</div>
					</div>
				</div>
				<div class="p-2 bd-highlight">
					<button class="button-start" onclick="newGame()">Начнём!</button>
				</div>
		</div>
	</div>
	</div>
`
	$("#screen").html(NewMenu);
}

chooseLevelScreen();
